﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
public class Ball : MonoBehaviour
{
    public float posX;
    public float posY;
 
    public float velX;
    public float velY;
 
    public float maxX;
    public float maxY;
    // Update is called once per frame
    void Update()
    {
        posY = transform.position.y + velY*Time.deltaTime;
 
        if(posY>maxY){
            posY = maxY;
            velY*=-1;
        }else if(posY<-maxY){
            posY = -maxY;
            velY*=-1;
        }
 
        posX = transform.position.x + velX*Time.deltaTime;
 
        if(posX>maxX){
            posX= maxX;
            velX*=-1;
        }else if(posX<-maxX){
            posX = -maxX;
            velX*=-1;
        }
 
        transform.position = new Vector3(posX, posY, transform.position.z);    
    }
}